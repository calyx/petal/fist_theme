require 'theme/config'
require_relative '../app/helpers/theme/theme_helper'

module Theme
  class Engine < ::Rails::Engine
    isolate_namespace Theme

    initializer "theme.helper" do |app|
      ActionController::Base.send :include, Theme::ThemeHelper
      ActionView::Base.send :include, Theme::ThemeHelper
    end

    initializer "theme.assets.precompile" do |app|
      app.config.assets.precompile << "application.css"
      Dir.chdir(File.expand_path('../../app/assets/', __FILE__)) do
        app.config.assets.precompile += Dir["fonts/*.woff2"]
      end
      Dir.chdir(File.expand_path('../../app/assets/images', __FILE__)) do
        app.config.assets.precompile += Dir["**/*.{svg,png,ico,jpg}"]
      end
    end
  end
end
